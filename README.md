# django2_usersignup
A simple website written in Django with a signup page where users can register for a new account before logging to the website.
It has a records app that incoporates the Django built-in authentication app.
----

This web site is using the Django built-in signup, login and logout system that is provided by its 
Core authentication framework (auth app) and its default models.

The main features that have currently been implemented arec:

* A signup page for users to register for an account.
* After a successful signup, user is redirected to the login page 
* Authentication pages are provided at the application level
* The auth app provides us with several authentication views and URLs for handling login, logout, and password management.
* Users accessing the home page are asked to use the provided login link in order to log include
* A successful login presents a welcome page with a logout link

Start the Django dev server with `py manage.py runserver` 

## Register for an account

Access the website and register for an account using `http://127.0.0.1:8000/records/signup/`

## Access the website

Access the website from the browser using `http://127.0.0.1:8000/accounts/login/`

## Reference

1.	[wsvincent](https://wsvincent.com/django-user-authentication-tutorial-signup/)

